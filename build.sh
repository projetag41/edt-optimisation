#!/bin/sh
export PATH_TO_FX=./lib/javafx-sdk-11.0.2/lib
if javac -target 11 -source 11 --module-path $PATH_TO_FX --add-modules javafx.graphics,javafx.controls,javafx.fxml -classpath .:./lib/json.jar -d bin/ -Xlint:none src/fr/utbm/ag41/*/*.java; then
	echo "Build complete !";
fi
