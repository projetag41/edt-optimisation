package fr.utbm.ag41.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.json.JSONObject;

/**
 * Class used to open JSON files
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class JSONUtils {
	/**
	 * Method that return a JSON file as a unique String
	 * @param path : path to the data file
	 * @return A String corresponding to the data stored in the file
	 */
	public static String getJSONSTringFromFile(String path) {
		File data = new File(path);
		Scanner scanner;
		String jsonString = null;
		try {
			scanner = new Scanner(data);
			jsonString = scanner.useDelimiter("\\Z").next();
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonString;
	}
	
	/**
	 * Transform a String into a JSON obbject
	 * @param path : Path to data file
	 * @return A JSON object corresponding to the data stored in the data file
	 */
	public static JSONObject getJSONObjetFromFile(String path) {
		return new JSONObject(getJSONSTringFromFile(path));
	}
	
	/**
	 * Method that check if the given object exist
	 * @param jsonObject : object to test
	 * @param key : key of the object
	 * @return A boolean depending of the existence of the jsonObject
	 */
	public static boolean objectExists(JSONObject jsonObject, String key) {
		Object object;
		try {
			object = jsonObject.get(key);
			return object != null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
