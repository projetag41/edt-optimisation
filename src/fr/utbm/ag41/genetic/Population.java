package fr.utbm.ag41.genetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ListIterator;
import java.util.Random;

import fr.utbm.ag41.datastructure.Formation;
import fr.utbm.ag41.datastructure.Interface;
import fr.utbm.ag41.timestructure.TaskList;
import fr.utbm.ag41.timestructure.TimeTable;

/**
 * Class that represent a Population in the context of a genetic algorithm
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Population {
	 
	/**
	 * List of Individual that store the population
	 */
	private ArrayList<Individual> pop;
	/**
	 * List of all formations
	 */
	ArrayList<Formation> formations;
	
	/**
	 * Constructor to initialize a population. It will create n Individual.
	 * @param size : Size of the population, so the number of Individual in it
	 * @param interfaceList : List of interfaces 
	 * @param formations : List of formations
	 */
	public Population(int size, ArrayList<Interface> interfaceList, ArrayList<Formation> formations){
		this.formations = formations;
		this.pop = new ArrayList<Individual>();
		//We create each individual
		for(int i=0;i<size;++i) {
			this.pop.add(new Individual(interfaceList, this.formations, 0));
		}
	}
	
	/**
	 * Get the list of individual that represent the population.
	 * @return An instance of an ArrayList<Individual>, representing the population
	 */
	public ArrayList<Individual> getPop(){
		return this.pop;
	}
	
	/**
	 * Get a specified individual in the population.
	 * @param index : the index of the individual to get
	 * @return An instance of an Individual
	 */
	public Individual getIndividual(int index) {
		return this.pop.get(index);
	}
	
	/**
	 * Method to select the n/2 (where n is the size of the population) best individual in the population.
	 * The selection method use the roulette wheel method : each individual is given a probability to be chose that is
	 * proportional to it adpation level.
	 * @return A list of individual corresponding to the choosen ones
	 */
	public ArrayList<Individual> selection(){
		double fitnessSum;
		double selectionValue = 0;
		double partialSum = 0;
		int size = this.pop.size();
		ArrayList<Individual> selected = new ArrayList<Individual>();
		
		//First we sort the population by adaptation level values, and we reverse it, to have the better one in first position
		Collections.sort(this.pop);
		Collections.reverse(this.pop);
		//We do this operation n/2 times
		for(int i=0;i<size/2;++i) {
			//The first step is to compute the total adaptation values sum, by avoiding individual already selected
			fitnessSum = 0.0;
			for(Individual indiv:this.pop) {
				if(!(selected.contains(indiv))) {
					fitnessSum = fitnessSum + indiv.getFitLvl();
				}
			}
			//Then we generate a random value between zero and 1
			partialSum = 0;
			selectionValue = Math.random();
			/*
			 * We start from the individual with the highest adaptation level value and, for each individual until the partialSum is higher
			 * than the random value, we add the adaptation values 
			 */
			for(Individual indiv:this.pop) {
				if(!(selected.contains(indiv))) {
					partialSum = partialSum + indiv.getFitLvl()/fitnessSum;
					if(partialSum > selectionValue) {
						selected.add(indiv);
						break;
					}
				}
			}
		}
		return selected;
	}
	
	/**
	 * Method to reproduce two individuals. It uses a multi-points crossover method : we look for each gene in the two parents, and according
	 * to a crossover probability, we cross gene or not.
	 * @param parent1 First parent
	 * @param parent2 Second parent
	 * @return A list of the two childs engendred by the crossover
	 */
	public ArrayList<Individual> crossover(Individual parent1, Individual parent2){
		int numberOfInterface = this.pop.get(0).getInterfaces().size();
		double crossOverProba = 0.70;
		double proba;
		Random rand = new Random();
		ArrayList<String> dayNames = new ArrayList<String>(Arrays.asList("LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"));
		String randDayName = dayNames.get(rand.nextInt(dayNames.size()));
		System.out.println(randDayName);
		
		ArrayList<Interface> child1Gene =  new ArrayList<Interface>();
		for (Interface interf : parent1.getInterfaces()) {
			child1Gene.add(new Interface(interf));
		}
		ArrayList<Interface> child2Gene =  new ArrayList<Interface>();
		for (Interface interf : parent2.getInterfaces()) {
			child2Gene.add(new Interface(interf));
		}
		TaskList t1,t2;
		
		/*
		 * For each gene(interface) we compare a random probability to the crossover probability.
		 * If it's higher, we cross the two genes in the childs DNA, if not, we just place them in the childs DNA
		 */
		for(int i=0;i<numberOfInterface;++i) {
			proba = Math.random();
			if(proba < crossOverProba) {
				t1 = new TaskList(child1Gene.get(i).getTimeTable().getDay(randDayName).getTaskList());
				t2 = new TaskList(child2Gene.get(i).getTimeTable().getDay(randDayName).getTaskList());
				child1Gene.get(i).getTimeTable().getDay(randDayName).setTaskList(t2);
				child2Gene.get(i).getTimeTable().getDay(randDayName).setTaskList(t1);
			}
		}
		
		ArrayList<Individual> indiv = new ArrayList<Individual>();
		Individual child1 = new Individual(child1Gene, this.formations, 1);
		Individual child2 = new Individual(child2Gene, this.formations, 1);
		
		indiv.add(child1);
		indiv.add(child2);
		return indiv;
	}
	
	/**
	 * Method to mutate a Population. Mutation is simple : two genes are swaped in an individual.
	 * @param mutationProbability : Probability for an individual to mutate
	 */
	public void mutation(double mutationProbability) {
		int firstMutatedGeneIndex, secondMutatedGeneIndex;
		Random rand = new Random();
		ArrayList<String> dayNames = new ArrayList<String>(Arrays.asList("LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"));
		String randDayName = dayNames.get(rand.nextInt(dayNames.size()));
		
		//We do it for each individual of the population
		for(Individual indiv:this.pop) {
			double probability = Math.random(); // We generate a random probability
			//If probability if lower than the mutation probability, we select two differents genes, and we swap them
			if(probability < mutationProbability) {
				firstMutatedGeneIndex = rand.nextInt(this.pop.get(0).getInterfaces().size());
				do {
					secondMutatedGeneIndex = rand.nextInt(this.pop.get(0).getInterfaces().size());
				}while(secondMutatedGeneIndex == firstMutatedGeneIndex);
				indiv.swapGene(firstMutatedGeneIndex, secondMutatedGeneIndex, randDayName);
			}
			
		}
	}
	
	/**
	 * Method to insert childs from a previous crossover in the population. We check each individual and if a child
	 * is better than an individual of the population, it dies and the child takes it place.
	 * @param newIndividual : List of new individual to insert in the population
	 */
	public void newGeneration(ArrayList<Individual> newIndividual) {
		ArrayList<Individual> toAdd = new ArrayList<Individual>();
		ListIterator<Individual> iter;
		Individual current;
		//First we sort the population and the new individuals by adaptation level values, and we reverse it, to have the better one in first position
		Collections.sort(newIndividual);
		Collections.reverse(newIndividual);
		Collections.sort(this.pop);
		Collections.reverse(this.pop);
		//Then, we compare each individual. We remove from the list the older ones that are less adapted than a child
		for(Individual indiv:newIndividual) {
		    iter = this.pop.listIterator();
			while(iter.hasNext()) {
				current = iter.next();
				if(indiv.getFitLvl() > current.getFitLvl()) {
					this.pop.remove(current);
					toAdd.add(indiv);
					break;
				}
			}
		}
		this.pop.addAll(toAdd); //We add all the better childs in the population
	}
	
}
