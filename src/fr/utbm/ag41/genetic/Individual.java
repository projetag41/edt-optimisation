package fr.utbm.ag41.genetic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import fr.utbm.ag41.datastructure.Formation;
import fr.utbm.ag41.datastructure.Interface;
import fr.utbm.ag41.timestructure.Day;
import fr.utbm.ag41.timestructure.Task;
import fr.utbm.ag41.timestructure.TaskList;
import fr.utbm.ag41.timestructure.TimeTable;

/**
 * Class that represent an individual in the context of a genetic algorithm.
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Individual implements Comparable<Individual>{
	/**
	 * List of interfaces (gene of the individual)
	 */
	private ArrayList<Interface> interfaces;
	/**
	 * List of formations
	 */
	private ArrayList<Formation> formations;
	/**
	 * Adaptation value of the individual
	 */
	private double fitLVl;
	
	/*
	 * Constructor of an Individual.
	 *  @param interfaceList : List of every interfaces
	 *  @param formationsList : List of every formation
	 *  @param mode : Their is two possibilities : if mode=0, it will create a random individual. This mode is used during the initialization of the population.
	 *  										   if mode=1, it will just create a individual by copying the interfaces given in parameter 
	 */
	public Individual(ArrayList<Interface> interfaceList, ArrayList<Formation> formationsList, int mode){
		//Initialization mode
		if(mode == 0) {
			//First, we shuffle the two list, to apply a first randomizer
			Collections.shuffle(interfaceList);
			Collections.shuffle(formationsList);
			
			this.interfaces = new ArrayList<Interface>();
			for (Interface interf : interfaceList) {
				this.interfaces.add(new Interface(interf));
			}
			this.formations = formationsList;
			
			/* This snippet is commented because we used copy constructor instead, it's more flexible and POO friendly
			 * This snippet of code is a little bit complex. Due to Java reference passing, the only way to have
			 * an independant copy of the interfaces list is to make what's called a "Deep Copy" : copying each 
			 * elements direclty in memory, not through Java API.
			 * A "simple" way to do that is to serialize the objects and pass them through a pipe. It will create
			 * an exact copy of the objects. After passing them in the pipe, we just have to rebuild the objects and
			 * put them in the destination list.
			 */
			/*for(Interface inter:interfaceList) {
				try{
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					ObjectOutputStream oos = new ObjectOutputStream(bos);
					oos.writeObject(inter);
					oos.flush();
					oos.close();
					bos.close();
					byte[] byteData = bos.toByteArray();
					ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
					Interface i = (Interface) new ObjectInputStream(bais).readObject();
					this.interfaces.add(i);
				}catch(IOException | ClassNotFoundException e){
					e.printStackTrace();
				}
			}*/
			
			Day formationDay;
			int hourStart, hourEnd , duration, choosenInterfaceIndex;
			double affectationProba = 0.50, proba;
			Random rand = new Random();
			
			/*
			 * For each formation, we generate a probability to be affected. If it's greater than the affectation probability,
			 * we affect this formation to a random interface by first transforming it to tasks.
			 */
			for(Formation formation:formationsList) {
				hourStart = formation.getStart();
				hourEnd = formation.getEnd();
				duration = hourEnd - hourStart;

				choosenInterfaceIndex = rand.nextInt(this.interfaces.size());
				Interface choosenInterface = this.interfaces.get(choosenInterfaceIndex);
				TimeTable tt = choosenInterface.getTimeTable();
				formationDay = tt.getDay(formation.getDay());
				if(formationDay.getTaskList().areTimeSlotEmpty(duration,hourStart)) {
					formationDay.getTaskList().insertTasks(formation.toTasks());
				}
			}
		//Creating a new individual with given genes
		}else if(mode == 1) {
			this.interfaces = interfaceList;
			this.formations = formationsList;
		}
		/*
		 * Finally, we compute the adaptation value of the individual. It avoids to use to much
		 * computational power and time.
		 */
		this.fitLVl = this.fitLvl();
	}
	/**
	 * Get the interface list that represent the genes of the individual
	 * @return A list of interfaces
	 */
	public ArrayList<Interface> getInterfaces(){
		return this.interfaces;
	}
	
	/**
	 * Get the adaptation level value
	 * @return A value corresponding to the adaptation value
	 */
	public double getFitLvl() {
		return this.fitLVl;
	}
	
	/**
	 * Method to compare two individual by adaptation level value. It is used when
	 * sorting a list of individual.
	 */
	@Override
	public int compareTo(Individual indiv1) {
		double fitLvl1 = indiv1.getFitLvl();
		
		if (fitLvl1 < this.getFitLvl()) {
	           return 1;
	       } else if (fitLvl1 > this.getFitLvl()){
	           return -1;
	       } else {
	           return 0;
	       }
	}
	
	/**
	 * Method to swap two genes of the individuals : it swaps two TaskList
	 * @param day Day on which we take the two genes
	 * @param gene1Index First gene index
	 * @param gene2Index Second gene index
	 */
	public void swapGene(int gene1Index, int gene2Index, String day) {
		TaskList firstTimeTable = new TaskList(this.interfaces.get(gene1Index).getTimeTable().getDay(day).getTaskList());
		TaskList secondTimeTable = new TaskList(this.interfaces.get(gene2Index).getTimeTable().getDay(day).getTaskList());
	
		this.interfaces.get(gene2Index).getTimeTable().getDay(day).setTaskList(firstTimeTable);
		this.interfaces.get(gene1Index).getTimeTable().getDay(day).setTaskList(secondTimeTable);
	}
	
	/**
	 * Method to compute the adaptation level value
	 * @return The adaptation level value of the individual
	 */
	public double fitLvl() {
		return c1() +c2() + c3()+ c4() + c5() + c6() + c7();
	}
	
	/**
	 * First constraint method : we check if each formation is affected.
	 * @return The amount of non affected formation. If all formations are
	 * affected, the method return 0
	 */
	private double c1() {
		Task actual, previous;
		int check;
		TaskList tl;
		ArrayList<TaskList> taskLists;
		ArrayList<Task> dayTasks;
		ArrayList<Formation> affected = new ArrayList<Formation>();
		//For each interface, we check every day of the week
		for (Interface inter:this.interfaces){
			System.out.println("Affected: " + affected.size());
			for(Day d:inter.getTimeTable().getDayList()) {
				d.getTaskList().sort();
				dayTasks = d.getTaskList().getTasks(); //TaskList of one day
				System.out.println("Tasks for " + d.getName() + " : " + dayTasks.size());
				taskLists = new ArrayList<TaskList>(); //We create an empty list to store tasks
				//We iterate through the day's tasklist to analyse each task
				for(int i=0;i < dayTasks.size();++i) {
					actual = dayTasks.get(i); //We get the actual task
					//If we are not on the first elements
					if(i-1 >= 0) {
						previous = dayTasks.get(i-1); //We get the previous element
						//If the actual task has the same infos that the previous one
						if(actual.getInfos().equals(previous.getInfos())){
							//We check all tasklist already created
							check = 0;
							for(TaskList list:taskLists) {
								//If the tasklist contains previous, we add it to it
								if(list.getTasks().contains(previous)) {
									list.addTask(actual);
								}else {
									check++;
								}
							}
							
							if(check == taskLists.size()) {
								tl = new TaskList();
								tl.addTask(actual);
								taskLists.add(tl);
							}
						//If it's differens infos, it's a different formation,so we create a new tasklist
						}else {
							tl = new TaskList();
							tl.addTask(actual);
							taskLists.add(tl);
						}
					//Else it's the first element, we create a new tasklist
					}else {
						tl = new TaskList();
						tl.addTask(actual);
						taskLists.add(tl);
					}
				}
				Formation current;
				//With each sub tasklist, we recreate formations, and we count each of the formation one time. 
				for(TaskList list: taskLists) {
					list.sort();
					current = list.toFormation();
					//System.out.println("Début: " + current.getStart() + ";Fin: " + current.getEnd() + ";Durée: " + (current.getEnd()-current.getStart()));
					if(!(affected.contains(current))) {
						affected.add(current);
					}
				}
			}
		}
		return affected.size(); //The result is the number of non affected formation
	}
	
	/**
	 * Second constraint method : Each interface is affected to formation with the correct skill
	 * @return The number of formation with the wrong skill
	 */
	private double c2() {
		int wrongSkill = 0;
		for (Interface inter:this.interfaces){
			for(Day d:inter.getTimeTable().getDayList()) {
				if(!d.getTaskList().getTasks().isEmpty()) {
					for(Task task: d.getTaskList().getTasks()) {
						if(!(task.getInfos().getSkill().equals(inter.getSkill()))) {
							wrongSkill++;
						}
					}
				}
			}
		}
		return wrongSkill;
	}
	
	/**
	 * Third constraint method : Each interface is affected to formation with the correct speciality
	 * @return The number of formation with the wrong speciality
	 */
	private double c3() {
		int wrongSpeciality = 0;
		for (Interface inter:this.interfaces){
			for(Day d:inter.getTimeTable().getDayList()) {
				if(!d.getTaskList().getTasks().isEmpty()) {
					for(Task task: d.getTaskList().getTasks()) {
						if(!(task.getInfos().getCenter().getSpeciality().equals(inter.getSpeciality()))) {
							wrongSpeciality++;
						}
					}
				}
			}
		}
		return wrongSpeciality;
	}
	
	/**
	 * Forth constraint method : Each formation is affected to only one interface
	 * @return The number of formation affected to two or more interfaces
	 */
	private double c4() {
		int wrongNumberOfFormation =0;
		Task actual, previous;
		int check;
		TaskList tl;
		ArrayList<TaskList> taskLists;
		ArrayList<Task> dayTasks;
		ArrayList<Formation> affected = new ArrayList<Formation>();
		for (Interface inter:this.interfaces){
			for(Day d:inter.getTimeTable().getDayList()) {
				dayTasks = d.getTaskList().getTasks(); //TaskList of one day
				taskLists = new ArrayList<TaskList>(); //We create an empty list to store tasks
				//We iterate through the day's tasklist to analyse each task
				for(int i=0;i < dayTasks.size();++i) {
					actual = dayTasks.get(i); //We get the actual task
					//If we are not on the first elements
					if(i-1 >= 0) {
						previous = dayTasks.get(i-1); //We get the previous element
						//If the actual task has the same infos that the previous one
						if(actual.getInfos().equals(previous.getInfos())){
							//We check all tasklist already created
							check = 0;
							for(TaskList list:taskLists) {
								//If the tasklist contains previous, we add it to it
								if(list.getTasks().contains(previous)) {
									list.addTask(actual);
								}else {
									check++;
								}
							}
							
							if(check == taskLists.size()) {
								tl = new TaskList();
								tl.addTask(actual);
								taskLists.add(tl);
							}
						//If it's differens infos, it's a different formation,so we create a new tasklist
						}else {
							tl = new TaskList();
							tl.addTask(actual);
							taskLists.add(tl);
						}
					//Else it's the first element, we create a new tasklist
					}else {
						tl = new TaskList();
						tl.addTask(actual);
						taskLists.add(tl);
					}
				}
				Formation current;
				/*
				 * With each sub tasklist, we recreate formations. If the it's affected to more than
				 * one interface, we increase by one the value of wrongNumberOfFormation
				 */
				for(TaskList list: taskLists) {
					list.sort();
					current = list.toFormation();
					if(!(affected.contains(current))) {
						affected.add(current);
					}else {
						wrongNumberOfFormation++;
					}
				}
			}
		}
		return wrongNumberOfFormation;
	}
	
	/**
	 * Fifth constraint method : Each interface work maximum 8 hours per day
	 * @return The number of day where the interfaces work more than 8 hours
	 */
	private double c5() {
		int wrongNumberOfHourPerDay = 0;
		for (Interface inter:this.interfaces) {
			for(Day d:inter.getTimeTable().getDayList()) {
				if (d.getTaskList().getTasks().size()>8){
					wrongNumberOfHourPerDay++;
				}
			}
		}
		return wrongNumberOfHourPerDay;
	}

	/**
	 * Sixth constraint method : Each interface work maximum 35 hour per week
	 * @return The number of interfaces that work more than 35 hours per week
	 */
	private double c6() {
		int wrongNumberOfHourPerWeek = 0, numberOfHourPerWeek;
		for (Interface inter:this.interfaces) {
			numberOfHourPerWeek = 0;
			for(Day d:inter.getTimeTable().getDayList()) {
				numberOfHourPerWeek+=d.getTaskList().getTasks().size();
			}
			if (numberOfHourPerWeek>35) {
				wrongNumberOfHourPerWeek++;
			}
		}
		return wrongNumberOfHourPerWeek;
	}

	/**
	 * Seventh constraint method : The distance travelled by each interfaces need to be harmonized.
	 * The gap between the distances travelled by each interfaces may not exceed 5%
	 * @return The number of gap exceeding 5% distance difference
	 */
	private double c7() {
		double dist1;
		double dist2;
		double relativeGap = 0;
		int notMinimizedDistance=0;
		for (Interface inter1:this.interfaces) {
			for (Interface inter2:this.interfaces) {
				if (!(inter1.equals(inter2))) {
					dist1 = inter1.computeTotalDistance();
					dist2 = inter2.computeTotalDistance();
					relativeGap = (Math.abs(dist1-dist2)/dist2)*100;
					if (relativeGap > 5) {
						notMinimizedDistance++;
					}
				}
			}
		}
		return notMinimizedDistance;
	}
}
