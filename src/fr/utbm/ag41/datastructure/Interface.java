package fr.utbm.ag41.datastructure;

import java.io.Serializable;

import fr.utbm.ag41.timestructure.Day;
import fr.utbm.ag41.timestructure.TaskList;
import fr.utbm.ag41.timestructure.TimeTable;

/**
 * Class that represent an interface
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Interface implements Serializable{
	
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Name of the interface
	 */
	private String name;
	/**
	 * Coordinates of the interface
	 */
	private Coord coord;
	/**
	 * TimeTable of the interface
	 */
	private TimeTable timetable;
	/**
	 * Speciality of the interface
	 */
	private String speciality;
	/**
	 * Skill of the interface
	 */
	private String skill;
	
	/**
	 * Class Constructor
	 * @param coord
	 * @param speciality
	 * @param skill
	 */
	public Interface(String name, Coord coord, String speciality, String skill){
		this.name = name;
		this.coord = coord;
		this.timetable = new TimeTable();
		this.speciality = speciality;
		this.skill = skill;
	}
	
	/**
	 * Class Copy Constructor
	 * @param interf
	 */
	public Interface(Interface interf) {
		this.name = new String(interf.name);
		this.coord = new Coord(interf.coord);
		this.timetable = new TimeTable(interf.timetable);
		this.speciality = new String(interf.speciality);
		this.skill = new String(interf.skill);
	}
	
	/**
	 * Get the name of the interface
	 * @return The name of the interface
	 */
	public String getName() {
		return name;
	}
	/**
	 * Get the coordinates of the interface
	 * @return A Coord corresponding to the coordinates of the interface
	 */
	public Coord getCoord() {
		return this.coord;
	}
	/**
	 * Get the timetable of the interface
	 * @return A TimeTable
	 */
	public TimeTable getTimeTable() {
		return this.timetable;
	}
	/**
	 * Set the TimeTable of the interface
	 * @param timetable : the TimeTable to set
	 */
	public void setTimeTable(TimeTable timetable) {
		this.timetable = timetable;
	}
	/**
	 * Get the interface's speciality
	 * @return A String corresponding to the interface's speciality
	 */
	public String getSpeciality() {
		return this.speciality;
	}
	/**
	 * Get the interface's skill
	 * @return A String corresponding to the interface's skill
	 */
	public String getSkill() {
		return this.skill;
	}
	/**
	 * Method that compute the total distance traveled by the interface
	 * @return The total distance traveled by the interface
	 */
	public double computeTotalDistance() {
		double totalDistance = 0;
		TaskList taskList;
		for(Day d:this.timetable.getDayList()) {
			taskList = d.getTaskList();
			for (int i=0;i<taskList.size()-1;++i) {
				if (i==0) {
					//Distance between the interface's house and the apprentice
					totalDistance+=Distance.computeDistance(this.coord, taskList.getTasks().get(i).getInfos().getApprentice().getCoord());
				}
				// Check if the next task is an other formation
				if (!(taskList.getTasks().get(i).getInfos().equals(taskList.getTasks().get(i+1).getInfos()))){
					// Apprentice house to Formation Center multiplied by 2, it's a round trip
					totalDistance+=Distance.computeDistance(taskList.getTasks().get(i).getInfos().getApprentice().getCoord(),taskList.getTasks().get(i).getInfos().getCenter().getCoord()) * 2;
					if (!(i==taskList.size()-1)) {
						// Distance from the Apprentice house to new Apprentice house
						totalDistance+=Distance.computeDistance(taskList.getTasks().get(i).getInfos().getApprentice().getCoord(),taskList.getTasks().get(i+1).getInfos().getApprentice().getCoord());
					}
				}
				if (i==taskList.size()-1) {
					// Apprentice house to interface house
					totalDistance+=Distance.computeDistance(taskList.getTasks().get(i).getInfos().getApprentice().getCoord(),this.coord);
				}
			}
		}
		return totalDistance;
	}
}
