package fr.utbm.ag41.datastructure;

/**
 * Class that represent a Formation Center
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class FormationCenter {
	
	/**
	 * Coordinates of the center
	 */
	private Coord coord;
	/**
	 * Speciality of the center
	 */
	private String speciality;
	
	/**
	 * Class Constructor
	 * @param coord
	 * @param speciality
	 */
	public FormationCenter(Coord coord, String speciality){
		this.coord = coord;
		this.speciality = speciality;
	}
	
	/**
	 * Class copy Constructor
	 * @param formationCenter
	 */
	public FormationCenter(FormationCenter formationCenter) {
		this.coord = new Coord(formationCenter.coord);
		this.speciality = new String(formationCenter.speciality);
	}
	
	/**
	 * Get the coordinates of the center
	 * @return Coord object corresponding to the coordinates of the center
	 */
	public Coord getCoord() {
		return this.coord;
	}
	/**
	 * Get the speciality of the center
	 * @return A string corresponding to the speciality of the center
	 */
	public String getSpeciality() {
		return this.speciality;
	}
}
