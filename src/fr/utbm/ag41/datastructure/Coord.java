package fr.utbm.ag41.datastructure;

import java.io.Serializable;
/**
 * Class that represent the coordinates of a node
 * @author root
 *
 */
public class Coord implements Serializable{
	
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * X coordinate
	 */
	private double x;
	/**
	 * y coordinate
	 */
	private double y;
	
	/**
	 * Class Constructor without parameter
	 */
	public Coord(){
		this.x = 0;
		this.y = 0;
	}
	
	/**
	 * Class Constructor
	 * @param x
	 * @param y
	 */
	public Coord(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Class Copy Constructor
	 * @param coord
	 */
	public Coord(Coord coord) {
		this.x = coord.x;
		this.y = coord.y;
	}
	
	/**
	 * Get x coordinate
	 * @return The x coordinate
	 */
	public double getX() {
		return this.x;
	}
	/**
	 * Set x coordinate
	 * @param x : New value of x
	 */
	public void setX(double x) {
		this.x = x;
	}
	/**
	 * Get y coordinate
	 * @return The y coordinate
	 */
	public double getY() {
		return this.y;
	}
	/**
	 * Set y coordinate
	 * @param y : New value of y
	 */
	public void setY(double y) {
		this.y = y;
	}
}