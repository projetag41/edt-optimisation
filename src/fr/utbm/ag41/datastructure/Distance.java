package fr.utbm.ag41.datastructure;

/**
 * Abstract class used to compute distance between two nodes
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public abstract class Distance {
	
	/**
	 * Compute the distance between two nodes
	 * @param node1 : First node
	 * @param node2 : Second node
	 * @return The distance between the two nodes
	 */
	public static double computeDistance(Coord node1, Coord node2) {
		return Math.sqrt(Math.pow(2 ,node2.getX() - node1.getX()) + Math.pow(2 ,node2.getY() - node1.getY()));
	}
}
