package fr.utbm.ag41.datastructure;

/**
 * Class that represent all the usefull informations about a formation
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class FormationInfo {
	
	/**
	 * Apprentice registered for the formation
	 */
	private Apprentice apprentice;
	/**
	 * Center where the formation take place
	 */
	private FormationCenter center;
	/**
	 * Skill required for the formation
	 */
	private String skill;
	/**
	 * Day of the formation
	 */
	private String day;
	
	/**
	 * Class constructor
	 * @param apprentice
	 * @param center
	 * @param skill
	 * @param day
	 */
	public FormationInfo(Apprentice apprentice, FormationCenter center, String skill, String day){
		this.apprentice = apprentice;
		this.center = center;
		this.skill = skill;
		this.day = day;
	}
	
	/**
	 * Classcopy constructor
	 * @param formationInfo
	 */
	public FormationInfo(FormationInfo formationInfo) {
		this.apprentice = new Apprentice(formationInfo.apprentice);
		this.center = new FormationCenter(formationInfo.center);
		this.skill = new String(formationInfo.skill);
		this.day = new String(formationInfo.day);
	}
	
	/**
	 * Get the apprentice registered for the formation
	 * @return : An Apprentice
	 */
	public Apprentice getApprentice() {
		return this.apprentice;
	}
	/**
	 * Get the center where the formation takes place
	 * @return A FormationCenter
	 */
	public FormationCenter getCenter() {
		return this.center;
	}
	/**
	 * Get the skill required for this formation
	 * @return A skill
	 */
	public String getSkill() {
		return this.skill;
	}
	/**
	 * Get the day of the formation
	 * @return A day
	 */
	public String getDay() {
		return this.day;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		
		FormationInfo info = (FormationInfo) obj;
		return this.apprentice == info.getApprentice() && this.center == info.getCenter() && this.day == info.getDay() && this.skill == info.getSkill();
	}

}
