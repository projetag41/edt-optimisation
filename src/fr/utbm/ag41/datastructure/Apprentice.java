package fr.utbm.ag41.datastructure;

/**
 * Class that represents an apprentice
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Apprentice {

	/**
	 * Name of the apprentice
	 */
	private String name;
	/**
	 * Coordinates of the apprentice
	 */
	private Coord coord;
	/**
	 * Deficiency (visual or auditive) of the apprentice
	 */
	private String deficiency;
	
	/**
	 * Class constructor
	 * @param coord
	 * @param deficiency
	 */
	public Apprentice(String name, Coord coord, String deficiency){
		this.name = name;
		this.coord = coord;
		this.deficiency = deficiency;
	}
	
	public Apprentice(Apprentice apprentice) {
		this.name = new String(apprentice.name);
		this.coord = new Coord(apprentice.coord);
		this.deficiency = new String(apprentice.deficiency);
	}
	
	/**
	 * Get the name of the apprentice
	 * @return The name of the apprentice
	 */
	public String getName() {
		return name;
	}
	/**
	 * Get the coordinates of the apprentice
	 * @return The coordinates of the apprentice
	 */
	public Coord getCoord() {
		return this.coord;
	}
	/**
	 * Get the deficiency of the apprentice
	 * @return A String corresponding to the deficiency of the apprentice
	 */
	public String getDeficiency() {
		return this.deficiency;
	}
	
	/**
	 * @return A String corresponding to the skill that match the defficiency
	 */
	public String getSkillNeeded() {
		if (this.deficiency.equals("visuelles")) {
			return "signes";
		}
		else if (this.deficiency.equals("auditives")){
			return "lpc";
		}
		return null;
	}
}
