package fr.utbm.ag41.datastructure;

import fr.utbm.ag41.timestructure.Task;
import fr.utbm.ag41.timestructure.TaskList;

/**
 * Class that represent a Formation
 * @author WITZ Francesco, SCMIDT Simon
 *
 */
public class Formation {
	
	/**
	 * Important infos of the formation
	 */
	private FormationInfo infos;
	/**
	 * Begin hour of the formation
	 */
	private int hourStart;
	/**
	 * End hour of the formation
	 */
	private int hourEnd;
	/**
	 * Day on which the formation take place
	 */
	private String day;

	/**
	 * Class Constructor
	 * @param infos
	 * @param hourStart
	 * @param hourEnd
	 * @param day
	 */
	public Formation(FormationInfo infos, int hourStart, int hourEnd, String day){
		this.infos = infos;
		this.day = day;
		this.hourStart = hourStart;
		this.hourEnd = hourEnd;
	}
	/**
	 * Get the important infos about the formation
	 * @return A FormationInfo object containing the important infos
	 */
	public FormationInfo getInfos() {
		return this.infos;
	}
	/**
	 * Get begin hour
	 * @return Start hour
	 */
	public int getStart() {
		return this.hourStart;
	}
	/**
	 * Get end hour
	 * @return End hour
	 */
	public int getEnd() {
		return this.hourEnd;
	}
	/**
	 * Get the day at which the formation take place
	 * @return A day
	 */
	public String getDay() {
		return this.day;
	}
	
	/**
	 * Method to transform a formation in a TaskList, to insert it in a timetable
	 * @return A TaskList corresponding to the formation
	 */
	public TaskList toTasks(){
		TaskList tasks = new TaskList();
		int numberOfTasks = hourEnd - hourStart;
		
		for(int i=0;i<numberOfTasks;++i) {
			tasks.addTask(new Task(this.hourStart + i, this.infos));
		}
		
		return tasks;
	}
	
}