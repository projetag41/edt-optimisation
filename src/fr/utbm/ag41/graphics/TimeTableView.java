package fr.utbm.ag41.graphics;

import java.util.Arrays;
import java.util.List;

import fr.utbm.ag41.datastructure.Interface;
import fr.utbm.ag41.timestructure.Day;
import fr.utbm.ag41.timestructure.Task;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class TimeTableView {
	/**
	 * Scene (what the window contains) of the application
	 */
	private Scene scene;
	/**
	 * Associated controller (MVC model)
	 */
	private TimeTableController controller;
	/**
	 * List of days (time table columns)
	 */
	private static List<String> dayStrings = Arrays.asList("LUNDI","MARDI","MERCREDI","JEUDI","VENDREDI","SAMEDI");
	/**
	 * List of time slots (time table rows)
	 */
	private static List<String> hourStrings = Arrays.asList("8h-9h","9h-10h","10h-11h","11h-12h","12h-13h","13h-14h","14h-15h","15h-16h","16h-17h","17h-18h");

	/**
	 * Pane where the interface name is displayed
	 */
	private StackPane topPane;
	/**
	 * Pane where the time table
	 */
	private GridPane centerPane;
	/**
	 * Label where the interface name is displayed
	 */
	private Label interfaceNameLabel;
	/**
	 * Button to switch to the previous interface
	 */
	private Button leftArrowButton;
	/**
	 * Button to switch to the next interface
	 */
	private Button rightArrowButton;
	
	/**
	 * Constructor of a TimeTableView (it creates the scene)
	 * @param controller - The associated controller
	 */
	public TimeTableView(TimeTableController controller) {
		this.controller = controller;
		this.buildScene();
	}
	/**
	 * Get the scene where everything is displayed
	 * @return The scene where everything is displayed
	 */
	public Scene getScene() {
		return scene;
	}
	/**
	 * Get the label where the interface name is displayed
	 * @return The label where the interface name is displayed
	 */
	public Label getInterfaceNameLabel() {
		return interfaceNameLabel;
	}
	/**
	 * Get the right button (switch to the previous interface)
	 * @return The right button (switch to the previous interface)
	 */
	public Button getLeftArrowButton() {
		return leftArrowButton;
	}
	/**
	 * Get the left button (switch to the next interface)
	 * @return The left button (switch to the next interface)
	 */
	public Button getRightArrowButton() {
		return rightArrowButton;
	}

	/**
	 * Create/build the scene where everything is displayed
	 */
	private void buildScene() {

		Color borderColor = new Color(0, 0, 0, 0.3);
		
		// Main pane : contains all the elements of the scene
		BorderPane mainPane = new BorderPane();
		
	
		this.topPane = new StackPane();
		topPane.setPadding(new Insets(5));
		this.interfaceNameLabel = new Label("");
		topPane.getChildren().add(this.interfaceNameLabel);
		
		StackPane leftArrowPane = new StackPane();
		this.leftArrowButton = new Button("<");
		this.leftArrowButton.setOnAction(this.controller::onLeftArrowActionned);
		leftArrowPane.setPadding(new Insets(10));
		leftArrowPane.getChildren().add(this.leftArrowButton);
		StackPane.setAlignment(this.leftArrowButton, Pos.CENTER_LEFT);
		
		StackPane rightArrowPane = new StackPane();
	    this.rightArrowButton = new Button(">");
	    this.rightArrowButton.setOnAction(this.controller::onRightArrowActionned);
		rightArrowPane.setPadding(new Insets(10));
		rightArrowPane.getChildren().add(this.rightArrowButton);
		StackPane.setAlignment(this.rightArrowButton, Pos.CENTER_RIGHT);
		
		
		this.centerPane = new GridPane();
		this.centerPane.setPadding(new Insets(20));
		this.centerPane.setHgap(1);
		this.centerPane.setVgap(1);
		
		// Add all the time slot labels (1st column)
		for (int hourIndex=1;hourIndex<TimeTableView.hourStrings.size()+1;++hourIndex) {
			Label tempLabel1 = new Label(TimeTableView.hourStrings.get(hourIndex-1));
			tempLabel1.setMinWidth(75);
			this.centerPane.add(tempLabel1, 0, hourIndex);
			GridPane.setHalignment(tempLabel1, HPos.CENTER);
		}
		
		// Add all the others columns
		for (int dayIndex=1;dayIndex<TimeTableView.dayStrings.size()+1;++dayIndex) {
			
				Label tempLabel2 = new Label(TimeTableView.dayStrings.get(dayIndex-1));
				this.centerPane.add(tempLabel2, dayIndex, 0);
				GridPane.setHalignment(tempLabel2, HPos.CENTER);
				
				for (int hourIndex=1;hourIndex<TimeTableView.hourStrings.size()+1;++hourIndex) {
					StackPane tempStackPane = new StackPane();
					tempStackPane.setPrefHeight(5000);
					tempStackPane.setPrefWidth(5000);
					tempStackPane.setBorder(new Border(new BorderStroke(borderColor, borderColor, borderColor, borderColor, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID,
							BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1), Insets.EMPTY)));
					Label tempLabel = new Label("");
					tempStackPane.getChildren().add(tempLabel);
					this.centerPane.add(tempStackPane, dayIndex, hourIndex);
				}
		}
		
		mainPane.setTop(topPane);
		mainPane.setLeft(leftArrowPane);
		mainPane.setRight(rightArrowPane);
		mainPane.setCenter(this.centerPane);
		
		this.scene = new Scene(mainPane,900,400);
	}
	
	/**
	 * Show or hide arrow buttons in function of the actual interface
	 * @param index : Index of the on screen interface
	 * @param listSize : Number of interfaces
	 */
	public void updateArrowButton(int index, int listSize) {
		if (index == 0){
			this.leftArrowButton.setVisible(false);
		}
		else if (index > 0)
		{
			this.leftArrowButton.setVisible(true);
		}
		
		if (index == listSize-1){
			this.rightArrowButton.setVisible(false);
		}
		else if (index < listSize-1)
		{
			this.rightArrowButton.setVisible(true);
		}
	}
	
	/**
	 * Change the content of the actual time table and the name of the interface
	 * @param interface1 : New interface on screen
	 */
	public void updateInterface(Interface interface1) {
		
		this.interfaceNameLabel.setText(interface1.getName()+" : "+interface1.getSkill()+" et "+interface1.getSpeciality());
		this.topPane.setBackground(null);
		this.topPane.setBackground(new Background(new BackgroundFill(this.getCellColor(interface1.getSpeciality()), CornerRadii.EMPTY, Insets.EMPTY)));
		
		int centerPaneIndex=0;
		int dayIndex=0;
		
		for (Day day : interface1.getTimeTable().getDayList()) {
			// Clean previous TimeTable
			for (int i=0;i<TimeTableView.hourStrings.size();++i) {
				centerPaneIndex = (TimeTableView.hourStrings.size()+1) + dayIndex*(TimeTableView.hourStrings.size()+1) + i;
				StackPane tempStackPane = ((StackPane)this.centerPane.getChildren().get(centerPaneIndex));
				tempStackPane.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
				((Label)tempStackPane.getChildren().get(0)).setText("");
			}
			// Print new TimeTable
			for (Task task : day.getTaskList().getTasks()) {
				centerPaneIndex = (TimeTableView.hourStrings.size()+1) + dayIndex*(TimeTableView.hourStrings.size()+1) + task.getHourStart()-8;
				
				StackPane tempStackPane = ((StackPane)this.centerPane.getChildren().get(centerPaneIndex));
				tempStackPane.setBackground(new Background(new BackgroundFill(this.getCellColor(task.getInfos().getCenter().getSpeciality()), CornerRadii.EMPTY, Insets.EMPTY)));
				
				String tempString = task.getInfos().getApprentice().getName()+" ("+task.getInfos().getApprentice().getSkillNeeded().substring(0, 3)+","+task.getInfos().getCenter().getSpeciality().substring(0, 3)+")";
				((Label)tempStackPane.getChildren().get(0)).setText(tempString);
			}
			dayIndex++;
		}
	}
	
	/**
	 * Get the color associated with a speciality.
	 * @param speciality : Speciality from which we want the color
	 * @return The color associated with the speciality.
	 */
	public Color getCellColor(String speciality) {
		switch (speciality) {
			case "sans":
				return Color.LIGHTGRAY;
			case "electricite":
				return Color.YELLOW;
			case "mecanique":
				return Color.LIGHTGREEN;
			case "menuiserie":
				return Color.LIGHTBLUE;
			default:
				return null;
		}
	}

}
