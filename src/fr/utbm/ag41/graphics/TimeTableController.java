package fr.utbm.ag41.graphics;

import java.util.ArrayList;

import fr.utbm.ag41.datastructure.Apprentice;
import fr.utbm.ag41.datastructure.Coord;
import fr.utbm.ag41.datastructure.FormationCenter;
import fr.utbm.ag41.datastructure.FormationInfo;
import fr.utbm.ag41.datastructure.Interface;
import fr.utbm.ag41.timestructure.Task;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

/**
 * Class that represents the controller of the time table view
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class TimeTableController {
	/**
	 * Stage (window) of the application
	 */
	private Stage stage;
	/**
	 * Associated view (MVC model)
	 */
	private TimeTableView view;
	/**
	 * List of interfaces from the best Individual
	 */
	private ArrayList<Interface> interfaces;
	/**
	 * Index of the on screen Interface
	 */
	private int interfaceIndex;
	

	/**
	 * Constructor of a TimeTableController (it creates the associated View)
	 */
	public TimeTableController() {
		this.view = new TimeTableView(this);
	}

	/**
	 * Begin the work of the controller by showing the scene
	 * @param stage : the stage of the application
	 * @param interfaces : list of interfaces from the best individual
	 */
	public void run(Stage stage,ArrayList<Interface> interfaces) {
		// TESTS
		/*Interface interface1 = new Interface("Jon Snow",new Coord(10.0,10.0), "mecanique", "lpc");
		interface1.getTimeTable().getDay("LUNDI").getTaskList().addTask(new Task(8, new FormationInfo(new Apprentice("Jean Michel",new Coord(20.0, -10.0),"visuelles"), new FormationCenter(new Coord(20.0, -10.0), "electricite"), "lpc", "LUNDI")));
		interface1.getTimeTable().getDay("LUNDI").getTaskList().addTask(new Task(10, new FormationInfo(new Apprentice("Luc Schmidt",new Coord(20.0, -10.0),"visuelles"), new FormationCenter(new Coord(20.0, -10.0), "menuiserie"), "lpc", "LUNDI")));
		interface1.getTimeTable().getDay("LUNDI").getTaskList().addTask(new Task(13, new FormationInfo(new Apprentice("Geralt de Riv",new Coord(20.0, -10.0),"visuelles"), new FormationCenter(new Coord(20.0, -10.0), "sans"), "lpc", "LUNDI")));
		Interface interface2 = new Interface("Tyrion Lannister",new Coord(10.0,10.0), "menuiserie", "signes");
		interface2.getTimeTable().getDay("MERCREDI").getTaskList().addTask(new Task(9, new FormationInfo(new Apprentice("Sansa Stark",new Coord(20.0, -10.0),"visuelles"), new FormationCenter(new Coord(20.0, -10.0), "mecanique"), "lpc", "LUNDI")));
		interface2.getTimeTable().getDay("MERCREDI").getTaskList().addTask(new Task(17, new FormationInfo(new Apprentice("Peter Baelish",new Coord(20.0, -10.0),"auditives"), new FormationCenter(new Coord(20.0, -10.0), "electricite"), "lpc", "LUNDI")));
		interface2.getTimeTable().getDay("SAMEDI").getTaskList().addTask(new Task(16, new FormationInfo(new Apprentice("Jaime Lannister",new Coord(20.0, -10.0),"visuelles"), new FormationCenter(new Coord(20.0, -10.0), "electricite"), "lpc", "LUNDI")));
		this.interfaces = new ArrayList<Interface>();
		this.interfaces.add(interface1);
		this.interfaces.add(interface2);*/
		this.interfaces = interfaces;
		this.interfaceIndex = 0;
		
		this.stage = stage;
		this.stage.setMinWidth(550);
		this.stage.setMinHeight(350);
		this.view.updateInterface(this.interfaces.get(this.interfaceIndex));
		this.view.updateArrowButton(this.interfaceIndex, this.interfaces.size());
		this.stage.setScene(this.view.getScene());
	}
	
	/**
	 * Change the on screen interface when the left arrow button is actionned
	 * @param e : associated event
	 */
	public void onLeftArrowActionned(ActionEvent e) {
		this.interfaceIndex--;
		this.view.updateInterface(this.interfaces.get(this.interfaceIndex));
		this.view.updateArrowButton(this.interfaceIndex, this.interfaces.size());
	}
	
	/**
	 * Change the on screen interface when the right arrow button is actionned
	 * @param e : associated event
	 */
	public void onRightArrowActionned(ActionEvent e) {
		this.interfaceIndex++;
		this.view.updateInterface(this.interfaces.get(this.interfaceIndex));
		this.view.updateArrowButton(this.interfaceIndex, this.interfaces.size());
	}
}
