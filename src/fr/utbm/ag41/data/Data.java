package fr.utbm.ag41.data;

import java.util.ArrayList;

import org.json.JSONObject;

import fr.utbm.ag41.datastructure.Apprentice;
import fr.utbm.ag41.datastructure.Coord;
import fr.utbm.ag41.datastructure.Formation;
import fr.utbm.ag41.datastructure.FormationCenter;
import fr.utbm.ag41.datastructure.FormationInfo;
import fr.utbm.ag41.datastructure.Interface;
import fr.utbm.ag41.json.JSONUtils;

/**
 * Class that contain method to read data files
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Data {
	
	/**
	 * List of apprentices
	 */
	private ArrayList<Apprentice> apprentices = new ArrayList<Apprentice>();
	/**
	 * List of formation centers
	 */
	private ArrayList<FormationCenter> formationCenters = new ArrayList<FormationCenter>();
	/**
	 * List of formations
	 */
	private ArrayList<Formation> formations = new ArrayList<Formation>();
	/**
	 * List of interfaces
	 */
	private ArrayList<Interface> interfaces = new ArrayList<Interface>();
	
	/**
	 * Method that return all the apprentice defined in the data file
	 * @return A list of Apprentice
	 */
	public ArrayList<Apprentice> getApprentices() {
		return apprentices;
	}
	/**
	 * Method that return all the formation centers defined in the data file
	 * @return A list of FormationCenter
	 */
	public ArrayList<FormationCenter> getFormationCenters() {
		return formationCenters;
	}
	/**
	 * Method that return all the formations defined in the data file
	 * @return A list of Formation
	 */
	public ArrayList<Formation> getFormations() {
		return formations;
	}
	/**
	 * Method that return all the interfaces defined in the data file
	 * @return A list of Interface
	 */
	public ArrayList<Interface> getInterfaces() {
		return interfaces;
	}

	/**
	 * Class constructor : read all the data stored in the json data file and create all the corresponding objects and store them.
	 * @param path : path to data files
	 */
	public Data(String path) {
		JSONObject object = JSONUtils.getJSONObjetFromFile(path);
		
		for (Object o : object.getJSONArray("apprentices")){
			this.apprentices.add(new Apprentice(((JSONObject)o).getString("name"),new Coord(((JSONObject)o).getDouble("x"),((JSONObject)o).getDouble("y")),((JSONObject)o).getString("deficiency")));
		}
		
		for (Object o : object.getJSONArray("formationCenters")){
			this.formationCenters.add(new FormationCenter(new Coord(((JSONObject)o).getDouble("x"),((JSONObject)o).getDouble("y")),((JSONObject)o).getString("speciality")));
		}
		
		for (Object o : object.getJSONArray("formations")){
			this.formations.add(new Formation(new FormationInfo(
					this.apprentices.get(((JSONObject)o).getInt("apprentice")),this.formationCenters.get(((JSONObject)o).getInt("formationcenter")),((JSONObject)o).getString("skill"),((JSONObject)o).getString("day")),((JSONObject)o).getInt("hourstart"),((JSONObject)o).getInt("hourend"),((JSONObject)o).getString("day")));
		}
		
		for (Object o : object.getJSONArray("interfaces")) {
			this.interfaces.add(new Interface(((JSONObject)o).getString("name"),new Coord(((JSONObject)o).getDouble("x"),((JSONObject)o).getDouble("y")),((JSONObject)o).getString("speciality"),((JSONObject)o).getString("skill")));
		}
	}
}