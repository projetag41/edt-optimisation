package fr.utbm.ag41.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.utbm.ag41.data.Data;
import fr.utbm.ag41.genetic.Individual;
import fr.utbm.ag41.genetic.Population;
import fr.utbm.ag41.graphics.TimeTableController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;


public class Main extends Application{
	
	private static ArrayList<ArrayList<Individual>> parents;
	private static ArrayList<Individual> childs;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Platform.setImplicitExit(true);
		primaryStage.setOnCloseRequest(e -> closeProgram());
		
		Parameters parameters = getParameters();
		
		List<String> args = parameters.getUnnamed();
		try{
			//First we open the file given in args
			Data data = new Data(args.get(0));
			

			int populationSize = 100; //Size Of population
			int numberOfGeneration = 100; //Number Of iteration
			int counter = 0;
			int size;
			int parent1Index;
			int parent2Index;
			Population population = new Population(populationSize, data.getInterfaces(), data.getFormations()); //We initialize the population
			ArrayList<Individual> selected = new ArrayList<Individual>();
			ArrayList<Integer> parentsIndex;
			final AtomicBoolean terminate = new AtomicBoolean(false);
			Random rand = new Random();
			
			long startTime = System.nanoTime(); //Taking the start time to measure execution time
			//We do it numberOfGeneration times
			while(counter < numberOfGeneration) {
				final CountDownLatch latch = new CountDownLatch((int)populationSize/4); //Object to synchronize
				childs = new ArrayList<Individual>();
				parents = new ArrayList<ArrayList<Individual>>();
				System.out.println("Génération " + (counter+1) + ":");
				System.out.println("Selection");
				//We select the individuals for the reproduction
				selected = population.selection();
				size = selected.size();
				//We make populationSize/4 differents couple for the reproduction
				parentsIndex = new ArrayList<Integer>();
				for(int i=0;i<size/2;++i) {
					do {
						parent1Index = rand.nextInt(size);
					}while(parentsIndex.contains(Integer.valueOf(parent1Index)));
					parentsIndex.add(Integer.valueOf(parent1Index));
					do {
						do {
							parent2Index = rand.nextInt(size);
						}while(parent2Index == parent1Index);
					}while(parentsIndex.contains(Integer.valueOf(parent2Index)));
					parentsIndex.add(Integer.valueOf(parent2Index));

					parents.add(new ArrayList<Individual>(Arrays.asList(selected.get(parent1Index), selected.get(parent2Index))));
				}
				//For the crossover, we used parallel computing to increase the efficacity of the algorithm
				System.out.println("Croisement");
				terminate.set(false);
				//For each couple, we create a Thread and we compute the crossover
				for(ArrayList<Individual> p : parents) {	
					new Thread() {
						public void run() {
							if(!terminate.get()) {
								synchronized(parents) {
									ArrayList<Individual> result = population.crossover(p.get(0), p.get(1));
									//We synchronize the access to the childs resource to avoid concurent modification
									synchronized(childs) {
										childs.addAll(result);
									}
								}
								latch.countDown(); //We decrease the latch
							}
						}
					}.start();
				}
				try {
			         latch.await();  //We await that the latch reach 0 to continue to the next step  
			         terminate.set(true);
			      } catch(InterruptedException e) {
			         System.out.println(e.getMessage());
			      }
				//We renew the population
				System.out.println("Renouvellement");
				population.newGeneration(childs);
				//we mutate the population
				System.out.println("Mutation");
				population.mutation(0.03);
				Collections.sort(population.getPop());
				Collections.reverse(population.getPop());
				for(Individual indiv:population.getPop()) {
					System.out.println(indiv.getFitLvl());
				}
				//We increase the counter and let's do it again !
				counter++;
			}
			long elapsedTime = System.nanoTime() - startTime; //We compute the elpased time
			System.out.println("\n Total execution time to solve the problem : " + elapsedTime/1000000000.0 + "s \n"); //Display the execution time
			//Sorting the population to get the better first
			Collections.sort(population.getPop());
			Collections.reverse(population.getPop());
			System.out.println("Best Individual : " + population.getPop().get(0).getFitLvl());//Displaying the best individual
			
			TimeTableController controller = new TimeTableController();
			
			controller.run(primaryStage,population.getPop().get(0).getInterfaces());
			primaryStage.show();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void closeProgram()
	{
		Platform.exit();
		System.exit(0);
	}

}
