package fr.utbm.ag41.timestructure;

import fr.utbm.ag41.datastructure.FormationInfo;

/**
 * Class that represent a one hour Task
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Task implements Comparable<Task>{

	/**
	 * Task begin hour
	 */
	private int hourStart;
	/**
	 * Information of the formation this task belong
	 */
	private FormationInfo infos;
	
	/**
	 * Class Constructor
	 * @param hourStart
	 * @param infos
	 */
	public Task(int hourStart, FormationInfo infos){
		this.hourStart = hourStart;
		this.infos = infos;
	}
	/**
	 * Class Copy Constructor
	 * @param task
	 */
	public Task(Task task) {
		this.hourStart = task.hourStart;
		this.infos = new FormationInfo(task.infos);
	}
	/**
	 * Get the begining of this task
	 * @return A int representing the begin hour
	 */
	public int getHourStart() {
		return this.hourStart;
	}
	/**
	 * Get the formation infos of this task
	 * @return A FormationInfo object containing the infos
	 */
	public FormationInfo getInfos() {
		return this.infos;
	}
	
	public void printInfos() {
		System.out.println("Jour: " + this.infos.getDay()+ ";Début: " + this.hourStart +"h");
	}
	
	/**
	 * Method overriden from interface Comparable. Used
	 * to compare two task in the aim of sorting a list of task
	 */
	@Override
	public int compareTo(Task task) {
		int hour = task.getHourStart();
		
		if (hour < this.getHourStart()) {
	           return 1;
	       } else if (hour >this.getHourStart()){
	           return -1;
	       } else {
	           return 0;
	       }
	}
	
}