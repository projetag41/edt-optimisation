package fr.utbm.ag41.timestructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import fr.utbm.ag41.datastructure.Formation;

/**
 * Class representing a list of tasks
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class TaskList implements Serializable{

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * List of tasks
	 */
	private ArrayList<Task> list;
	
	/**
	 * Constructor
	 */
	public TaskList(){
		this.list = new ArrayList<Task>();
	}
	
	/**
	 * Copy Constructor
	 * @param taskList
	 */
	public TaskList(TaskList taskList){
		this.list = new ArrayList<Task>();
		for (Task task : taskList.list) {
			this.list.add(new Task(task));
		}
	}
	
	/**
	 * Get the tasks stored in the task list
	 * @return A list of Task
	 */
	public ArrayList<Task> getTasks(){
		return this.list;
	}
	
	/**
	 * Method to add the Task task in the task list
	 * @param task : Task to add
	 */
	public void addTask(Task task) {
		if(this.isTimeSlotEmpty(task.getHourStart())) {
			this.list.add(task);
		}
	}
	
	/**
	 * Method to remove the Task task of the list
	 * @param task : Task to remove
	 */
	public void removeTask(Task task) {
		this.list.remove(task);
	}
	
	/**
	 * Method to insert a TaskList in the list
	 * @param tasks :List of Task to insert
	 */
	public void insertTasks(TaskList tasks) {
		for(Task t:tasks.getTasks()) {
			this.addTask(t);
		}
	}
	
	/*
	 * Method to get the size of the list
	 */
	public int size() {
		return this.list.size();
	}
	
	/**
	 * Method to check if the time slot starting at the given hour is empty
	 * @param hour : Begin hour of the time slot
	 * @return true if the time slot is empty, false otherwise
	 */
	public boolean isTimeSlotEmpty(int hour) {
		if(this.list.isEmpty()) {
			return true;
		}else {
			for(Task t:this.list) {
				if(t.getHourStart() != hour) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Method to check if the time slots from the time slot starting at the given hour during duration are empty
	 * @param hourStart : Begin hour of the time slot
	 * @param duration : Number of time slot to check
	 * @return true if the time slots are empty, false otherwise
	 */
	public boolean areTimeSlotEmpty(int duration, int hourStart) {
		boolean slotsFree = true;
		if(!this.list.isEmpty()) {
			for(Task t:this.list) {
				for (int i=0;i<=duration;++i) {
					if(t.getHourStart() == hourStart+i) {
						slotsFree = false;
					}
				}
			}
			return slotsFree;
		}else {
			return true;
		}
	}
	
	/*
	 * Method to sort a task list
	 */
	public void sort() {
		Collections.sort(this.list);
	}
	
	/**
	 * Method to transform a TaskList into a Formation
	 * @param day : Day of the formation
	 * @return The rebuilded formation
	 */
	public Formation toFormation() {
		Formation formation = new Formation(this.list.get(0).getInfos(), this.list.get(0).getHourStart(),this.list.get(list.size()-1).getHourStart()+1, this.list.get(0).getInfos().getDay());
		return formation;
	}
	
	/**
	 * Method to get an entire formation by giving only one task
	 * @param task : The Task to search
	 * @return The corresponding formation
	 */
	public Formation getFormationByTask(Task task) {
		Formation found;
		TaskList tasksFound = new TaskList();
		for(Task t: this.list) {
			if(t.getInfos().equals(task.getInfos())) {
				tasksFound.addTask(t);
			}
		}
		tasksFound.sort();
		found = tasksFound.toFormation();
		return found;
	}
}
