package fr.utbm.ag41.timestructure;

import java.io.Serializable;
/**
 * Class that represent a Day
 * @author WITZ Francesco, SCHMIDT Simon
 *
 */
public class Day implements Serializable{
	
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Day name
	 */
	private String name;
	/**
	 * All the task of the day
	 */
	private TaskList taskList;
	
	/**
	 * Class Constructor
	 * @param name
	 */
	public Day(String name){
		this.name = name;
		this.taskList = new TaskList();
	}
	
	/**
	 * Class Copy Constructor
	 * @param day
	 */
	public Day(Day day){
		this.name = new String(day.name);
		this.taskList = new TaskList(day.taskList);
	}
	
	/**
	 * Get the day name
	 * @return A String corresponding to day name
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * Get all the task of the day
	 * @return A list of tasks
	 */
	public TaskList getTaskList(){
		return this.taskList;
	}
	/**
	 * Set the tasks of the day
	 * @param taskList : Tasks to set 
	 */
	public void setTaskList(TaskList taskList) {
		this.taskList = taskList;
	}
	/**
	 * Get the number of work hour in the day
	 * @return Number of work hour in the day
	 */
	public int getDayWorkHoursNumber() {
		return this.taskList.size();
	}
}
