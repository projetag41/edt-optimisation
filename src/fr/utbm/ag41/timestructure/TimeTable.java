package fr.utbm.ag41.timestructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class that represent the timetable of an interface
 * @author root
 *
 */
public class TimeTable implements Serializable{
	
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * List of day representing a week of work
	 */
	private ArrayList<Day> dayList;
	
	/**
	 * Class Constructor
	 * We create 6 new days
	 */
	public TimeTable(){
		this.dayList = new ArrayList<Day>();
		ArrayList<String> dayNames = new ArrayList<String>(Arrays.asList("LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"));
		for(String dayName: dayNames) {
			this.dayList.add(new Day(dayName));
		}
	}
	
	/**
	 * Class Copy Constructor
	 * @param timeTable
	 */
	public TimeTable(TimeTable timeTable) {
		this.dayList = new ArrayList<Day>();
		for(Day day : timeTable.dayList) {
			this.dayList.add(new Day(day));
		}
	}
	
	/**
	 * Get a specific day in the time table
	 * @param name : Name of the day to get
	 * @return The specified day if it exist
	 */
	public Day getDay(String name) {
		for(Day d:dayList) {
			if(d.getName().equals(name)){
				return d;
			}
		}
		return null;
	}
	/**
	 * Return the day list
	 * @return A list of Day
	 */
	public ArrayList<Day> getDayList() {
		return dayList;
	}
}
