# EDT Optimisation

Project made for the AG41 UTBM's UV during the 2020 spring semester. It consist in the optimization of 
the timetable of worker's who helps disabled persons at assisting different formations. 
The optimization take account of the amount of distance to travel between the formation centers 
and the people houses, the maximum time of work in a day, the specialities of
the workers, and an harmonization between all workers to avoid some to travel more than others

## Installation and Requirements

To install this project, you just need to clone the repository.
Make sure you have Java 11 Runtime Environnement installed on your machine.

## Running

First, you need to compile the project by executing the **build.sh** script.
Then, to run it, you just have to launch the **run.sh <file.json>** script. The argument of the **run.sh** script is **an absolute path to a JSON file**, correctly formated as explained bellow.

## Creating a data file

Here is a simple example of JSON file that will be read by the programm :

```json
{
    "apprentices":
    [
        {"x":-15.8,"y":140.5,"deficiency":"visuelles"},
        {"x":105.2,"y":0.0,"deficiency":"auditives"},
        {"x":55.2,"y":-10.0,"deficiency":"auditives"},
        {"x":78.42,"y":-15.8,"deficiency":"visuelles"}
    ],
    "formationCenters":
    [
        {"x":28.3,"y":9.5,"speciality":"sans"},
        {"x":98.13,"y":-952.5,"speciality":"electricite"},
        {"x":-22.0,"y":19.0,"speciality":"mecanique"},
        {"x":-84.3,"y":48.1,"speciality":"menuiserie"}
    ],
    "formations":
    [
        {"apprentice":0,"formationcenter":2,"skill":"lpc","day":"LUNDI","hourstart":8,"hourend":11},
        {"apprentice":1,"formationcenter":3,"skill":"signes","day":"MERCREDI","hourstart":13,"hourend":16},
        {"apprentice":2,"formationcenter":2,"skill":"signes","day":"SAMEDI","hourstart":8,"hourend":10},
        {"apprentice":3,"formationcenter":1,"skill":"lpc","day":"JEUDI","hourstart":16,"hourend":18},
        {"apprentice":2,"formationcenter":2,"skill":"signes","day":"MERCREDI","hourstart":14,"hourend":16},
        {"apprentice":3,"formationcenter":0,"skill":"lpc","day":"LUNDI","hourstart":16,"hourend":18},
        {"apprentice":1,"formationcenter":0,"skill":"signes","day":"MARDI","hourstart":10,"hourend":12},
        {"apprentice":0,"formationcenter":2,"skill":"lpc","day":"VENDREDI","hourstart":10,"hourend":12},
        {"apprentice":3,"formationcenter":1,"skill":"lpc","day":"VENDREDI","hourstart":13,"hourend":16},
        {"apprentice":3,"formationcenter":2,"skill":"lpc","day":"MERCREDI","hourstart":8,"hourend":10},
        {"apprentice":3,"formationcenter":1,"skill":"lpc","day":"VENDREDI","hourstart":14,"hourend":16},
        {"apprentice":1,"formationcenter":2,"skill":"signes","day":"MARDI","hourstart":11,"hourend":12},
        {"apprentice":0,"formationcenter":2,"skill":"lpc","day":"LUNDI","hourstart":13,"hourend":14},
        {"apprentice":2,"formationcenter":3,"skill":"signes","day":"SAMEDI","hourstart":13,"hourend":16},
        {"apprentice":0,"formationcenter":0,"skill":"lpc","day":"MARDI","hourstart":17,"hourend":18},
        {"apprentice":0,"formationcenter":0,"skill":"lpc","day":"VENDREDI","hourstart":17,"hourend":18},
        {"apprentice":0,"formationcenter":2,"skill":"lpc","day":"MARDI","hourstart":9,"hourend":10},
        {"apprentice":3,"formationcenter":1,"skill":"lpc","day":"LUNDI","hourstart":11,"hourend":12},
        {"apprentice":3,"formationcenter":1,"skill":"lpc","day":"SAMEDI","hourstart":8,"hourend":9}
    ],
    "interfaces":
    [
        {"x":23.0,"y":89.5,"speciality":"sans","skill":"lpc"},
        {"x":-18.5,"y":95.5,"speciality":"electricite","skill":"signes"},
        {"x":98.9,"y":-96.8,"speciality":"mecanique","skill":"signes"},
        {"x":8.6,"y":9.0,"speciality":"menuiserie","skill":"signes"},
        {"x":-4.33,"y":0.0,"speciality":"electricite","skill":"lpc"},
        {"x":14.7,"y":-13.5,"speciality":"mecanique","skill":"lpc"},
        {"x":58.0,"y":5.5,"speciality":"menuiserie","skill":"lpc"}
    ]
}
```


