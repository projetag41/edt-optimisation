#!/bin/sh
cd bin/
export PATH_TO_FX=../lib/javafx-sdk-11.0.2/lib
if [ $# -eq 0 ];then
	echo "Il faut préciser un fichier .json en argument !"
	exit 1
else
	java -cp ".:../lib/*" --module-path $PATH_TO_FX --add-modules javafx.graphics,javafx.controls,javafx.fxml fr.utbm.ag41.main.Main $1
fi
